
    
@extends('layout')

@section('content')


    <h3> Welcome to Laravel Practice </h3>

    <h4> Users </h4>

    <ul> 
        <?php foreach( $users as $user ):  ?>
            <li> <?php echo $user->name ?> </li>

        <?php endforeach  ?>
    </ul>

    <h4> Posts </h4>
    <div>
        <?php foreach( $posts as $post ):  ?>

        <p>
            @ <strong> <?php echo $post->user . "</strong>" . $post->content ?>
        </p> 
        <?php endforeach  ?>
    </div>    


@endsection




