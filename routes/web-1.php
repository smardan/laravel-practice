<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Faker\Factory;



class Post {}
class User {}

Route::get('/about' , function(){
    return view('about');
});

Route::get('/', function () {

    $faker = Factory::create();

    // Making a manual data as associative array
    // $data = [ 
    //     'users' => [] ,
    //     'posts' => []
    // ];
    
    //In laravel, members of data arrays are automatically converted into variables
    // If we were in plan php, for that we had to do this line:
    // extract($data), and then take $data members into var_dump world.

    // Then we make things a bit more advanced by writing instances of classes we
    // defined outside of this view function:

    $user1 = new User();
    $user1->handle = $faker->username;
    $user1->name = $faker->name;
    //$user1->dateJoined = ' ';
    
    $user2 = new User();
    $user2->handle = $faker->username;
    $user2->name = $faker->name;
    
    $post1 = new Post();
    $post1->user = $user1->handle;
    //$post1->datePosted = '2019-04-18';
    $post1->content = $faker->text(280);

    $post2 = new Post();
    $post2->user = $user2->handle;
    //$post2->datePosted = '2019-04-18';
    $post2->content = $faker->text(550);

    $data = [ 
        'users' => [$user1 , $user2 ] ,
        'posts' => [$post1 , $post2 ]
    ];


    return view('index' , $data);
});


Route::get('/welcome', function () {
    return view('welcome');
});
